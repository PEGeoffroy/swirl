"use strict";

let main = document.getElementById("main");

/**
 * @param {Number} size
 * @return {Array}
 */
function createMap(size: number): Array<Array<number>> {
    if (size % 2 === 0) {
        size++;
    }
    let array: Array<Array<number>> = [];
    for (let i = 0; i < size; i++) {
        array.push([]);
        for (let y = 0; y < size; y++) {
            array[i].push(getRandomInt(2));
        }
    }
    consoleMap(array);
    displayMap(array);
    return array;
}

/**
 * @param {Array} array
 */
function consoleMap(array: Array<Array<number>>) {
    for (let i = 0; i < array.length; i++) {
        let row = "";
        for (let y = 0; y < array[i].length; y++) {
            row += array[i][y];
        }
        console.log(row);
    }
}

/**
 * @param {Number} step
 * @param {Number} total
 * @return {Number}
 */
function getType(step: number, total: number = 3): number {
    if (step >= total) {
         return 1;
    } else {
        step++
        return step;
    }
}

/**
 * @param {Number} size
 * @return {Array}
 */
function getArrayCropPart(size: number): Array<string> {
    let array = [];
    let step = 0;
    let row = (size - 1) / 2;
    for (let i = 0; i < row; i++) {
        for (let y = 0; y < row - step; y++) {
            array.push(`${i}${y}`);
            array.push(`${size - i - 1}${size - y - 1}`);
        }
        step++;
    }
    console.log(array);
    return array;
}

/**
 * @param {Number} start
 * @return {Number}
 */
function getRandomInt(end: number = 1): number {
    return Math.floor(Math.random() * (end + 1));
}

/**
 * @param {Array} array
 */
function displayMap(array: Array<Array<number>>, w: number = 90,) {
    let cropArray = getArrayCropPart(array.length);

    let tileW = 100; // Ma ref
    let tileH = (75 * tileW) / 100; // Abs ou rel ?
    // let headTile = (50 * tileW) / 100;

    // let intervalW = (13.39745962 * 100) / tileW;

    let rowH   = (43.30127019 * 100) / tileW;
    let rowW   = (23.20508076 * 100) / tileW;
    let cellH  = (31.69872981 * 100) / tileW;
    let cellW  = (63.39745962 * 100) / tileW;
    let little = (86.60254038 * 100) / tileW;

    let mapW = (array.length - 1) * little + tileW;
    let mapH = array.length * tileW;

    let cropW = (((array.length - 1) / 2) * w) - (w - rowW - cellW) * (((array.length - 1) / 2) - 1);
    let cropH = ((((array.length - 1) / 2) * tileH) - ((1/100 * tileW) * (((array.length - 1) / 2) - 1)) - (3 * tileH) - ((1/100 * tileW) * 3)) / 2;

    let map = document.createElement("div");
    map.setAttribute("id", "map");
    map.style.width = `${mapW}px`;
    map.style.height = `${mapH - 480}px`;

    let step = 1;
    for (let i = 0; i < array.length; i++) {
        for (let y = 0; y < array[i].length; y++) {
            if (cropArray.indexOf(`${i}${y}`) === -1) {
                let tile = "";
                let cell = document.createElement("div");
                let img = document.createElement("img");
                cell.classList.add("cell");
                cell.style.top = `${(i * rowH + y * cellH) - cropH - 92}px`;
                cell.style.left = `${((array.length * rowW + array.length * cellW) - i * rowW + y * cellW) - cropW - 243}px`;
                img.style.width = `${tileW}px`;
                if (array[i][y] === 0) img.src = `./src/img/png/grass_${step}.png`;
                if (array[i][y] === 1) img.src = `./src/img/png/soil_${step}.png`;
                if (array[i][y] === 2) img.src = `./src/img/png/water.png`;
                img.alt = tile;
                cell.classList.add(`${i}:${y}`);
                cell.appendChild(img);
                step = getType(step);
                map.appendChild(cell);
            }
        }
    }
    main!.appendChild(map);
}

let mapTest = createMap(9);
