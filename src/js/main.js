"use strict";
var main = document.getElementById("main");
/**
 * @param {Number} size
 * @return {Array}
 */
function createMap(size) {
    if (size % 2 === 0) {
        size++;
    }
    var array = [];
    for (var i = 0; i < size; i++) {
        array.push([]);
        for (var y = 0; y < size; y++) {
            array[i].push(getRandomInt(2));
        }
    }
    consoleMap(array);
    displayMap(array);
    return array;
}
/**
 * @param {Array} array
 */
function consoleMap(array) {
    for (var i = 0; i < array.length; i++) {
        var row = "";
        for (var y = 0; y < array[i].length; y++) {
            row += array[i][y];
        }
        console.log(row);
    }
}
/**
 * @param {Number} step
 * @param {Number} total
 * @return {Number}
 */
function getType(step, total) {
    if (total === void 0) { total = 3; }
    if (step >= total) {
        return 1;
    }
    else {
        step++;
        return step;
    }
}
/**
 * @param {Number} size
 * @return {Array}
 */
function getArrayCropPart(size) {
    var array = [];
    var step = 0;
    var row = (size - 1) / 2;
    for (var i = 0; i < row; i++) {
        for (var y = 0; y < row - step; y++) {
            array.push("" + i + y);
            array.push("" + (size - i - 1) + (size - y - 1));
        }
        step++;
    }
    console.log(array);
    return array;
}
/**
 * @param {Number} start
 * @return {Number}
 */
function getRandomInt(end) {
    if (end === void 0) { end = 1; }
    return Math.floor(Math.random() * (end + 1));
}
/**
 * @param {Array} array
 */
function displayMap(array, w) {
    // let tileH = 72/100 * tileW;
    if (w === void 0) { w = 90; }
    // let rowW = 23/100 * tileW;
    // let rowH = 41/100 * tileW;
    // let cellW = 64/100 * tileW;
    // let cellH = 31/100 * tileW;
    var cropArray = getArrayCropPart(array.length);
    // let mapW = 1;
    // let mapH = 1;
    // let cropW = 1;
    // let cropH = 1
    // let mapW = (array.length * tileW) - (tileW - rowW - cellW) * (array.length - 1);
    // let mapH = (array.length * tileH) - ((1/100 * tileW) * (array.length - 1)) - (3 * tileH) - ((1/100 * tileW) * 3);
    // let cropW = (((array.length - 1) / 2) * tileW) - (tileW - rowW - cellW) * (((array.length - 1) / 2) - 1);
    // let cropH = ((((array.length - 1) / 2) * tileH) - ((1/100 * tileW) * (((array.length - 1) / 2) - 1)) - (3 * tileH) - ((1/100 * tileW) * 3)) / 2;
    // let mapH = (array.length * tileW) - (tileW - rowW - cellW) * (array.length - 1);
    var tileW = 100; // Ma ref
    var tileH = (75 * tileW) / 100; // Abs ou rel ?
    var headTile = (50 * tileW) / 100;
    var intervalW = (13.39745962 * 100) / tileW;
    var rowH = (43.30127019 * 100) / tileW;
    var rowW = (23.20508076 * 100) / tileW;
    var cellH = (31.69872981 * 100) / tileW;
    var cellW = (63.39745962 * 100) / tileW;
    var little = (86.60254038 * 100) / tileW;
    var mapW = (array.length - 1) * little + tileW;
    var mapH = array.length * tileW;
    var cropW = (((array.length - 1) / 2) * w) - (w - rowW - cellW) * (((array.length - 1) / 2) - 1);
    var cropH = ((((array.length - 1) / 2) * tileH) - ((1 / 100 * tileW) * (((array.length - 1) / 2) - 1)) - (3 * tileH) - ((1 / 100 * tileW) * 3)) / 2;
    var map = document.createElement("div");
    map.setAttribute("id", "map");
    map.style.width = mapW + "px";
    map.style.height = mapH - 480 + "px";
    var step = 1;
    for (var i = 0; i < array.length; i++) {
        for (var y = 0; y < array[i].length; y++) {
            if (cropArray.indexOf("" + i + y) === -1) {
                var tile = "";
                var cell = document.createElement("div");
                var img = document.createElement("img");
                cell.classList.add("cell");
                cell.style.top = (i * rowH + y * cellH) - cropH - 92 + "px";
                cell.style.left = ((array.length * rowW + array.length * cellW) - i * rowW + y * cellW) - cropW - 243 + "px";
                img.style.width = tileW + "px";
                if (array[i][y] === 0)
                    img.src = "./src/img/png/grass_" + step + ".png";
                if (array[i][y] === 1)
                    img.src = "./src/img/png/soil_" + step + ".png";
                if (array[i][y] === 2)
                    img.src = "./src/img/png/water.png";
                img.alt = tile;
                cell.classList.add(i + ":" + y);
                cell.appendChild(img);
                step = getType(step);
                map.appendChild(cell);
            }
        }
    }
    main.appendChild(map);
}
var mapTest = createMap(9);
