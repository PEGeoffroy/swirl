# Swirl

> Swirl is a JS rogue-lite.

***

Swirl is using :

* Lite-server
* SASS
* concurrently
* font-awesome

***

```sh
sudo npm install -g typescript
npm install
npm run all
```

It will launch:

* A sass watcher
* A lite server

To minify the SASS lauch:

```sh
npm run sass-minify
```
